import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightGreen[300],
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(height: 10, color: Colors.white),
            _Navbar(),
            Container(height: 25, color: Colors.white),
            _LoginHeader(),
            _LoginForm()
          ],
        ),
      ),
    );
  }
}

class _Navbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30),
      color: Colors.white,
      alignment: Alignment.bottomLeft,
      child: SafeArea(
        child: Text(
          "Collec'try",
          style: TextStyle(
              fontFamily: 'Libre', fontSize: 25, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}

class _LoginHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.white,
          ),
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(27),
            bottomRight: Radius.circular(27),
          ) // use instead of BorderRadius.all(Radius.circular520))
          ),
      padding: EdgeInsets.only(bottom: 50),
      child: Center(
        child: Image.asset(
          'assets/images/recycling_login.png',
          height: 200,
        ),
      ),
    );
  }
}

class _LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20, top: 15, left: 30, right: 30),
      alignment: Alignment.bottomLeft,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 15),
          Text(
            "Join us!",
            style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
          ),
          Text("recyclons ensemble", style: TextStyle(fontSize: 20)),
          SizedBox(height: 25),
          buildEmail(),
          SizedBox(height: 10),
          buildPassword(),
          SizedBox(height: 20),
          Container(
            width: 1000,
            child: submitButton(),
          ),
          SizedBox(height: 15),
          Center(
            child: Text('Ou'),
          ),
          SizedBox(height: 15),
          Center(child: registerButton(context))
        ],
      ),
    );
  }
}

Widget buildEmail() => TextField(
      decoration: InputDecoration(
        labelText: 'Email',
        suffixIcon: Icon(Icons.mail_outline),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
    );

Widget buildPassword() => TextField(
      decoration: InputDecoration(
        labelText: 'Password',
        suffixIcon: Icon(Icons.lock_outline_rounded),
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
    );

Widget submitButton() => TextButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.all(20)),
        backgroundColor: MaterialStateProperty.all(Colors.black),
      ),
      onPressed: null,
      child: Text(
        "Login",
        style: TextStyle(color: Colors.white),
      ),
    );

Widget registerButton(BuildContext context) => TextButton(
      child: Text(
        "S'inscrire",
        style: TextStyle(color: Colors.black, fontSize: 17),
      ),
      onPressed: () {
        Navigator.of(context).pushNamed('/register');
      },
    );
