import 'package:flutter/material.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            SizedBox(height: 20),
            _Header(),
            SizedBox(height: 50),
            _Form(),
            SizedBox(height: 15),
            Center(child: Text('Ou')),
            SizedBox(height: 15),
            loginButton(context),
          ],
        ),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SafeArea(
            child: Text(
              "Créér une nouvelle compte",
              style: TextStyle(color: Colors.lightGreen[500], fontSize: 27),
            ),
          ),
          Text("Creer un compte pour commencer à reclicler")
        ],
      ),
    );
  }
}

class _Form extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          _buildFirstname(),
          SizedBox(height: 25),
          _buildEmail(),
          SizedBox(height: 25),
          _buildPassword(),
          SizedBox(height: 25),
          _buildEmail(),
          SizedBox(height: 25),
          Container(width: 2000, child: submitButton()),
        ],
      ),
    );
  }
}

Widget _buildFirstname() => TextField(
      decoration: InputDecoration(
        labelText: 'Nom',
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        filled: true,
        fillColor: Colors.grey[100],
      ),
    );

Widget _buildEmail() => TextField(
      decoration: InputDecoration(
        labelText: 'Email',
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        filled: true,
        fillColor: Colors.grey[100],
      ),
    );

Widget _buildPassword() => TextField(
      decoration: InputDecoration(
        labelText: 'Password',
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white),
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        filled: true,
        fillColor: Colors.grey[100],
      ),
    );

Widget submitButton() => TextButton(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.all(20)),
        backgroundColor: MaterialStateProperty.all(Colors.lightGreen[500]),
      ),
      onPressed: null,
      child: Text(
        "S'inscrire",
        style: TextStyle(color: Colors.white, fontSize: 17),
      ),
    );

Widget loginButton(BuildContext context) => TextButton(
      child: Text(
        "Se loguer",
        style: TextStyle(color: Colors.black, fontSize: 17),
      ),
      onPressed: () {
        Navigator.of(context).pushNamed('/login');
      },
    );
